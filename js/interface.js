var headerItems = document.querySelectorAll(".user-nav__list > .user-nav__item");

for(let i = 0; i<headerItems.length;i++) {
	headerItems[i].querySelector(".popup-open").addEventListener("click", function() {
	
		if(headerItems[i].querySelector(".popup-wrapper").querySelector(".popup__close")){
			var popupCloseBtn = headerItems[i].querySelector(".popup__close");
				popupCloseBtn.addEventListener("click", function() {
					headerItems[i].querySelector(".popup-wrapper").classList.add("visually-hidden");
				} )
		}

		headerItems[i].querySelector(".popup-wrapper").classList.toggle("visually-hidden");	

})

}   

$(".message__close").click(function() {
	$(this).parent().hide()
})
	
// }

$(document).mouseup(function (e){ // отслеживаем событие клика по веб-документу
      
        var block = $(".popup-wrapper");
         // определяем элемент, к которому будем применять условия (можем указывать ID, класс либо любой другой идентификатор элемента)
        if (!block.is(e.target) // проверка условия если клик был не по нашему блоку
            && block.has(e.target).length === 0) { // проверка условия если клик не по его дочерним элементам
           	
            block.addClass("visually-hidden"); // если условия выполняются - скрываем наш элемент

				}
     })	


    
// ==================== user-popup startup =====================

var startupList = document.querySelector(".item--startup")

startupList.addEventListener("click",function() {
	if(this.classList.contains("opened")) {
		this.classList.remove("opened");
		this.style.marginBottom = 0;
	}
	else
		this.classList.add("opened");
} )

// ==================== user-popup startup end =====================

// ==================== lang ================

var currentLang = document.querySelector(".current-lang");
var langList = document.querySelectorAll(".popup__lang");
	
for(let i = 0 ;i<langList.length;i++) {
	langList[i].addEventListener("click", function() {
		let tmp = currentLang.textContent;
		currentLang.textContent = langList[i].textContent;
		langList[i].textContent = tmp;
		document.querySelector(".popup-wrapper--lang").classList.add("visually-hidden");

	})
}


// ==================== lang end================

// ==================== balance bttons  =====================

var labelControls = document.querySelectorAll(".order__balance-label");

var prevChoice = 0;

for(let i = 0 ; i< labelControls.length;i++) {
	labelControls[i].addEventListener("click", function() {
		
		if(!labelControls[i].classList.contains("selected")) {
			labelControls[i].classList.add("selected")
			labelControls[prevChoice].classList.remove("selected");
			prevChoice = i;

		}
	})
};

// ==================== balance bttons  end=====================

// ==================== percent help=====================
var percentBtn = document.querySelectorAll(".percent-help");
for(let i = 0 ; i< percentBtn.length;i++) {
	percentBtn[i].addEventListener("click", function() {
	if(percentBtn[i].querySelector(".percent__popup").style.display === "block")
		percentBtn[i].querySelector(".percent__popup").style.display = "none";
	else
		percentBtn[i].querySelector(".percent__popup").style.display = "block"

	})
}

// ==================== percent help end=====================

$(document).mouseup(function (e){ // отслеживаем событие клика по веб-документу
      
        var block = $(".percent__popup");
         // определяем элемент, к которому будем применять условия (можем указывать ID, класс либо любой другой идентификатор элемента)
        if (!block.is(e.target) // проверка условия если клик был не по нашему блоку
            && block.has(e.target).length === 0) { // проверка условия если клик не по его дочерним элементам
           	
            block.hide(); // если условия выполняются - скрываем наш элемент

				}
     })	



// ==================== checkboxes & connections  with roadmap bttons  =====================
var checkboxes = document.querySelectorAll(".order__option");

for(let i = 0; i < checkboxes.length; i++) {
	if(checkboxes[i].querySelector(".checkbox-label")) {
	checkboxes[i].querySelector(".checkbox-label").addEventListener("click",function() {
		if(checkboxes[i].querySelector(".checkbox-label").classList.contains("checked"))
			checkboxes[i].querySelector(".checkbox-label").classList.remove("checked")
		else
			checkboxes[i].querySelector(".checkbox-label").classList.add("checked")

	})
	checkboxes[i].querySelector(".option__label").addEventListener("click",function() {
		if(checkboxes[i].querySelector(".checkbox-label").classList.contains("checked"))
			checkboxes[i].querySelector(".checkbox-label").classList.remove("checked")
		else
			checkboxes[i].querySelector(".checkbox-label").classList.add("checked")

	})
}
}

// checkbox - roadmap connection

var checkboxBlocks = document.querySelectorAll(".option__checkbox");

for(let i = 0; i < checkboxBlocks.length;i++) {
	checkboxBlocks[i].onchange  = function() {
		if(checkboxBlocks[i].checked === true) {
			document.querySelector("." + checkboxBlocks[i].dataset.connection).querySelector(".option__status-bar").style.border = "1px solid #31dee0"
		}
		else
			document.querySelector("." + checkboxBlocks[i].dataset.connection).querySelector(".option__status-bar").style.border = "1px solid #d9d9d9"	
	}
}

// ==================== checkboxes & connections  with roadmap bttons  end =====================
